//
//  ViewController.h
//  RecipePList
//
//  Created by Adam Farrell on 5/26/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InformationViewController.h"

@interface ViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    NSArray* arrayOfRecipes;
}

@end

