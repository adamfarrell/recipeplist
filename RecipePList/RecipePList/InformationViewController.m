//
//  InformationViewController.m
//  RecipePList
//
//  Created by Adam Farrell on 5/26/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import "InformationViewController.h"

@interface InformationViewController ()

@end

@implementation InformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    float width = self.view.bounds.size.width;
    float height = self.view.bounds.size.height;
    float offset = 20.0f;
    self.view.backgroundColor = [UIColor lightGrayColor];
    
    UIImageView* image = [[UIImageView alloc]initWithFrame:CGRectMake(offset, 75, width - (offset * 2), height / 4)];
    [image setContentMode:UIViewContentModeScaleAspectFit];
    [image setImage:[UIImage imageNamed:[self.recipeDictionary objectForKey:@"image"]]];
    [self.view addSubview:image];
    
    
    UILabel* titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(offset / 2, (height / 4) + 85, width - offset, 40)];
    titleLabel.text = [self.recipeDictionary objectForKey:@"title"];
    [titleLabel setFont:[UIFont boldSystemFontOfSize:24]];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.numberOfLines = 1;
    titleLabel.adjustsFontSizeToFitWidth = YES;
    [self.view addSubview:titleLabel];
    
    UITextView* description = [[UITextView alloc]initWithFrame:CGRectMake(offset, (height / 4) + 135, width - (offset * 2), height / 2)];
    [description setText:[[self.recipeDictionary objectForKey:@"description"] stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"]];
    [description setTextColor:[UIColor whiteColor]];
    [description setBackgroundColor:[UIColor lightGrayColor]];
    [description setFont:[UIFont systemFontOfSize:16]];
    [self.view addSubview:description];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
