//
//  ViewController.m
//  RecipePList
//
//  Created by Adam Farrell on 5/26/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor lightGrayColor];
    
    NSString* filePath = [[NSBundle mainBundle]pathForResource:@"RecipePropertyList" ofType:@"plist"];
    arrayOfRecipes = [[NSArray alloc]initWithContentsOfFile:filePath];
    
    UITableView* tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:tableView];

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrayOfRecipes.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    NSDictionary* recipeDictionary = arrayOfRecipes[indexPath.row];
    
    cell.textLabel.text = [recipeDictionary objectForKey:@"title"];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.backgroundColor = [UIColor lightGrayColor];
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary* recipeDictionary = arrayOfRecipes[indexPath.row];
    InformationViewController* recipe = [InformationViewController new];
    recipe.recipeDictionary = recipeDictionary;
    [self.navigationController pushViewController:recipe animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
